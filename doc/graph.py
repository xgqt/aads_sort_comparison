#!/usr/bin/env python


"""
Graph the gathered data.
"""


import numpy as np
import matplotlib.pyplot as pl


def main():
    """Main."""

    # data to plot
    n_groups = 18
    res_bubble = (0.151955, 0.61413, 0.501931, 0.497654, 2.42247, 2.01675,
                  1.12412, 5.43716, 4.58731, 2.02529, 10.0617, 8.40862,
                  3.21313, 15.6204, 13.3571, 4.8583, 22.9557, 19.4255)
    res_insert = (0.000033, 0.147673, 0.0728885, 0.000065, 0.572939, 0.292053,
                  0.0001018, 1.3076, 0.783877, 0.0001317, 3.09757, 1.58224,
                  0.000159, 4.30277, 2.17056, 0.0002735, 6.14736, 3.12526)
    res_mergeb = (0.0017288, 0.00200188, 0.0024918, 0.00369789, 0.00368738,
                  0.00523726,
                  0.00517778, 0.00520234, 0.00815137, 0.0075691, 0.00742639,
                  0.012296,
                  0.0108685, 0.0118919, 0.0174882, 0.0131733, 0.0125205,
                  0.0177369)
    res_merget = (0.00114412, 0.00109708, 0.00182964, 0.00247362, 0.00282691,
                  0.00428802,
                  0.00342393, 0.00332912, 0.00569161, 0.00439622, 0.00410906,
                  0.00747628,
                  0.00580764, 0.0058508, 0.0106542, 0.00824503, 0.00688372,
                  0.012481)
    res_quick = (0.615576, 0.359819, 0.00186535, 2.41012, 1.43412, 0.00369624,
                 5.41868, 3.25145, 0.00611946, 9.66683, 5.72252, 0.00818759,
                 15.0369, 9.03589, 0.0109634, 23.096, 14.1549, 0.0136958)
    res_select = (0.125525, 0.130422, 0.123898, 0.48807, 0.510549, 0.494096,
                  1.1458, 1.15937, 1.10968, 1.9889, 2.09681, 2.00558,
                  3.00206, 3.28637, 3.24191, 4.42219, 4.72807, 4.49135)

    # create plot
    fig, ax = pl.subplots()
    index = np.arange(n_groups)
    bar_width = 0.15
    opacity = 0.8

    rects1 = pl.bar(index + bar_width * 0, res_bubble, bar_width,
                    alpha=opacity,
                    color="#ff0000",
                    label="Bubble")
    rects2 = pl.bar(index + bar_width * 1, res_insert, bar_width,
                    alpha=opacity,
                    color="#00ff00",
                    label="Insert")
    rects3 = pl.bar(index + bar_width * 2, res_mergeb, bar_width,
                    alpha=opacity,
                    color="#0000ff",
                    label="Merge Bottom-Top")
    rects4 = pl.bar(index + bar_width * 3, res_merget, bar_width,
                    alpha=opacity,
                    color="#00ffff",
                    label="Merge Top-Bottom")
    rects5 = pl.bar(index + bar_width * 4, res_quick, bar_width,
                    alpha=opacity,
                    color="#ff00ff",
                    label="Quick")
    rects6 = pl.bar(index + bar_width * 5, res_select, bar_width,
                    alpha=opacity,
                    color="#ffff00",
                    label="Select")

    pl.xlabel("Elements (x*10000)")
    pl.ylabel("Time (s)")
    pl.title("Sort Algorithms - Bar Graph")
    pl.xticks(index + bar_width,
              ("1 sorted", "1 reverse", "1 random",
               "2 sorted", "2 reverse", "2 random",
               "3 sorted", "3 reverse", "3 random",
               "4 sorted", "4 reverse", "4 random",
               "5 sorted", "5 reverse", "5 random",
               "6 sorted", "6 reverse", "6 random"))
    pl.legend()

    pl.tight_layout()
    pl.show()


if __name__ == "__main__":
    main()

#include "linear.hpp"


int linear ( int * array, const int size, const int wanted )
{
    for ( int i = 0; i < size; i ++ )

        // jeśli zawartość listy pod indeksem i jest równa wanted
        // zwróć ten numer indeksu
        if ( array[i] == wanted )
            return i;

    // jeśli wartość nie zastanie znaleziona zwróć -1
    // wybieramy -1 ponieważ taki indeks "nie istnieje"
    return -1;
}

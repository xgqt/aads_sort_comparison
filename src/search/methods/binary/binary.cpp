#include "binary.hpp"


int binary ( int * array, const int size, const int wanted )
{
    int begin = 0;
    int end   = size - 1;

    while ( begin <= end )
        {
            int middle = (begin + end) / 2;

            if ( array[middle] == wanted )
                return middle;

            // jeśli element w środku jest większy od wanted
            if ( array[middle] > wanted )
                // pierwsza połowa
                end = middle - 1;
            else
                // druga połowa
                begin = middle + 1;
        }

    // jeśli wartość nie zastanie znaleziona zwróć -1
    // wybieramy -1 ponieważ taki indeks "nie istnieje"
    return -1;
}

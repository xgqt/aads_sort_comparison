#include <chrono>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include "methods/binary/binary.hpp"
#include "methods/linear/linear.hpp"


int random_element ( int size )
{
    return rand() % size;
}

void benchchmark_sort
( int * array, int array_size, int wanted, int (*fun)(int *, int, int) )
{
    // start licznika
    auto start = std::chrono::high_resolution_clock::now();

    // wynik funkcji `fun' (funkcji podanej do tej funkcj)
    int ret = fun(array, array_size, wanted);

    // stop licznika
    auto stop = std::chrono::high_resolution_clock::now();

    if ( ret == -1 )
        {
            std::cerr << "No element was found!\n";
        }
    else
        {
            std::cout << "Found element: " << wanted
                      << " on index: [" << ret << "]\n";
        }

    // Obliczanie czasu który upłynął
    auto duration = std::chrono::duration_cast <std::chrono::nanoseconds> (stop - start).count();

    std::cout << ">>> Time elapsed in ns: " << duration << "\n";
}


int main ( )
{
    int sizes[] = { 10000, 20000, 30000, 40000, 50000, 60000 };

    for ( int sample_list_size : sizes )
        {
            int * sample_list = new int [sample_list_size];

            // posortowana lista
            for ( int i = 0; sample_list_size > i; i ++ )
                {
                    sample_list[i] = i;
                }

            int first  = sample_list[0];
            int middle = sample_list[sample_list_size / 2];
            int last   = sample_list[sample_list_size - 1];

            std::cout << "\n------------------------------\n";
            std::cout << "List size: " << sample_list_size << "\n";

            std::cout << "Linear:\n";
            std::cout << "- element: " << first << " (first) \n";
            benchchmark_sort(sample_list, sample_list_size, first,  linear);
            std::cout << "- element: " << middle << " (middle) \n";
            benchchmark_sort(sample_list, sample_list_size, middle, linear);
            std::cout << "- element: " << last << " (last) \n";
            benchchmark_sort(sample_list, sample_list_size, last,   linear);

            std::cout << "----------\n";

            std::cout << "Binary:\n";
            std::cout << "- element: " << first << " (first) \n";
            benchchmark_sort(sample_list, sample_list_size, first,  binary);
            std::cout << "- element: " << middle << " (middle) \n";
            benchchmark_sort(sample_list, sample_list_size, middle, binary);
            std::cout << "- element: " << last << " (last) \n";
            benchchmark_sort(sample_list, sample_list_size, last,   binary);

            delete [] sample_list;
        }


    std::cout << "\n\nPress ENTER to exit the program.\n\n";
    getchar();

    return 0;
}

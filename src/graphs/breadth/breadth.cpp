#include <iostream>
#include <queue>


// liczba wierzchołków
const int n = 8;


void print ( int d[], int p[] )
{
    // Stos
    int S [n];
    // Wskaźnik stosu
    int sptr = 0;

    for ( int i = 0; i < n; i ++ )
        {
            std::cout << i << ": ";

            // Ścieżkę przechodzimy od końca ku początkowi,
            // Zapisując na stosie kolejne wierzchołki
            for ( int j = i; j > -1; j = p[j] )
                S[sptr++] = j;

            // Wyświetlamy ścieżkę, pobierając wierzchołki ze stosu
            while ( sptr )
                std::cout << S[-- sptr] << " ";

            // Na końcu ścieżki wypisujemy jej koszt
            std::cout << "dist = " << d[i] << "\n";
        }
}

void BFS ( int G[][n], int s )
{
    // Tworzymy tablicę odwiedzin
    bool visited[n];

    // Tworzymy tablicę odległości
    int d[n];

    // Tworzymy tablicę poprzedników
    int p[n];

    for ( int i = 0; i < n; i ++ )
        {
            // inicjalizacja tablic
            visited[i] = false;
            d[i] = -1;
            p[i] = -1;
        }

    visited[s] = true;
    d[s] = 0;

    std::queue<int> Q;
    Q.push(s);

    while ( !Q.empty() )
        {
            // Odczytujemy u z kolejki
            int u = Q.front();

            // Usuwamy z kolejki odczytane u
            Q.pop();

            std::cout << "wiercholek " << u << "\n";

            for ( int v = 0; v < n; v ++ )
                if ( G[u][v] == 1 && !visited[v] )
                    {
                        visited[v] = true;
                        d[v] = d[u] + 1;
                        p[v] = u;
                        Q.push(v);
                    }
        }

    print(d, p);
}


int main ( )
{
    int graf[n][n] = {
        { 0, 1, 0, 0, 1 },
        { 1, 0, 0, 0, 0, 1 },
        { 0, 0, 0, 1, 0, 1, 1 },
        { 0, 0, 1, 0, 0, 0, 1, 1 },
        { 1 },
        { 0, 1, 1, 0, 0, 0, 1 },
        { 0, 0, 0, 1, 0, 1, 0, 1 },
        { 0, 0, 0, 1, 0, 0, 1 }
    };

    // wierzchołek początkowy
    int s = 3;
    BFS(graf, s);


    std::cout << "\n\nPress ENTER to exit the program.\n\n";
    getchar();

    return 0;
}

// Algorytm Dijkstry z wyszukiwaniem liniowym


#include <iostream>
#include <limits>


const int MAXINT = std::numeric_limits<int>::max();

int main()
{
    // liczba wierzchołków
    const int n = 5;

    // macierz sąsiedztwa
    int graf[n][n] = {
        { 0, 5, 10, 0, 0 },
        { 0, 0, 3, 2, 9 },
        { 0, 2, 0, 0, 1 },
        { 7, 0, 0, 0, 6 },
        { 0, 0, 0, 4, 0 }
    };

    // Tablica kosztów dojścia
    int d[n];
    // Tablica poprzedników
    int p[n];
    // Zbiory Q
    bool Q[n];

    // węzeł startowy
    int s = 1;

    // Inicjalizujemy tablicę
    for ( int i = 0; i < n; i ++ )
        {
            d[i] = MAXINT;
            p[i] = -1;
            Q[i] = false;
        }

    // Koszt dojścia s jest zerowy
    d[s] = 0;

    // Wyznaczamy ścieżki

    for ( int i = 0; i < n; i ++ )
        {
            // Szukamy wierzchołka w Q o najmniejszym koszcie d
            int min_d;

            for ( min_d = 0; Q[min_d]; min_d ++ );

            for ( int j = min_d + 1; j < n; j ++ )
                if ( !Q[j] && (d[j] < d[min_d]) )
                    min_d = j;

            // Znaleziony wierzchołek "usuwamy" z kolejki
            Q[min_d] = true;

            // Modyfikujemy odpowiednio wszystkich sąsiadów min_d
            for ( int j = 0; j < n; j ++ )
                if ( graf[min_d][j] != 0 && d[j] > d[min_d] + graf[min_d][j] )
                    {
                        d[j] = d[min_d] + graf[min_d][j];
                        p[j] = min_d;
                    }
        }


    // wyświetlenie wyników

    // Stos
    int S[n];
    // Wskaźnik stosu
    int sptr = 0;

    for ( int i = 0; i < n; i ++ )
        {
            std::cout << i << ": ";

            // Ścieżkę przechodzimy od końca ku początkowi,
            // Zapisując na stosie kolejne wierzchołki
            for ( int j = i; j > -1; j = p[j] )
                S[sptr ++] = j;

            // Wyświetlamy ścieżkę, pobierając wierzchołki ze stosu
            while ( sptr )
                std::cout << S[-- sptr] << " ";

            // Na końcu ścieżki wypisujemy jej koszt
            std::cout << "\nCost: " << d[i] << "\n\n";
        }


    std::cout << "\n\nPress ENTER to exit the program.\n\n";
    getchar();

    return 0;
}

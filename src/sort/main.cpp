#include <chrono>
#include <iostream>
#include <stdlib.h>
#include <time.h>

#include "methods/bubble/bubble.hpp"
#include "methods/insert/insert.hpp"
#include "methods/mergeb/mergeb.hpp"
#include "methods/merget/merget.hpp"
#include "methods/quick/quick.hpp"
#include "methods/select/select.hpp"


// testowane rozmiary
const int list_sizes [] = { 10000, 20000, 30000, 40000, 50000, 60000 };


int random_element ( int size )
{
    // from 0 to size
    return rand() % size;
}

void print_elapsed ( int * array, int size, void (*fun)(int *, int) )
{
    // zliczenie czasu przed wykonaniem funkcji
    auto start = std::chrono::high_resolution_clock::now();

    // wykonanie funkcji
    fun(array, size);

    // zliczenie czasu po wykonaniu funkcji
    auto stop = std::chrono::high_resolution_clock::now();

    // obliczanie czasu który upłynął
    auto duration = std::chrono::duration_cast <std::chrono::nanoseconds> (stop - start).count();
    std::cout << ">>> Time elapsed (s): " << duration / 1000000000.00  << "\n";
}

void benchchmark_sort ( void (*fun)(int *, int) )
{
    for ( int sample_list_size : list_sizes )
        {
            /// Inicjalizacja list

            int * sample_list_sorted  = new int [sample_list_size];
            int * sample_list_reverse = new int [sample_list_size];
            int * sample_list_random  = new int [sample_list_size];

            // posortowana lista
            for ( int i = 0; sample_list_size > i; i ++ )
                {
                    sample_list_sorted[i] = i;
                }

            // posortowana lista w odwrotnej kolejności
            for ( int i = 0; sample_list_size > i; i ++ )
                {
                    sample_list_reverse[i] = sample_list_size - i - 1;
                }

            // lista z losowymi elementami
            for ( int i = 0; sample_list_size > i; i ++ )
                {
                    sample_list_random[i] = random_element(sample_list_size);
                }

            std::cout << "[S] Sorted list                ["
                      << sample_list_size << "]\n";
            print_elapsed(sample_list_sorted,  sample_list_size, fun);

            std::cout << "[R] Sorted list, reverse order ["
                      << sample_list_size << "]\n";
            print_elapsed(sample_list_reverse, sample_list_size, fun);

            std::cout << "[E] List with random elements  ["
                      << sample_list_size << "] \n";
            print_elapsed(sample_list_random,  sample_list_size, fun);
        }
}


int main ( )
{
    // Ziarno do generowania laosowych liczb
    srand(time(NULL));

    std::cout << "\n---  Bubble sort method            ---\n";
    benchchmark_sort(bubble);

    std::cout << "\n---  Insert sort method            ---\n";
    benchchmark_sort(insert);

    std::cout << "\n---  Merge Bottom-Up sort method   ---\n";
    benchchmark_sort(mergeb);

    std::cout << "\n---  Merge Top-Bottom sort method  ---\n";
    benchchmark_sort(merget);

    std::cout << "\n---  Quick sort method             ---\n";
    benchchmark_sort(quick);

    std::cout << "\n---  Select sort method            ---\n";
    benchchmark_sort(select);


    std::cout << "\n\nPress ENTER to exit the program.\n\n";
    getchar();

    return 0;
}

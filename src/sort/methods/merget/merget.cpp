/// Merge Top-Down


#include "merget.hpp"


void merge_array ( int l, int m, int r, int A[], int B[] )
{
    int i, j, k;

    if (r - l < 1)
        return;

    i = k = l;
    j = m;

    while (i < m && j <= r)
        B[k++] = (A[i] <= A[j]) ? A[i++] : A[j++];

    while (i < m)
        B[k++] = A[i++];

    while (j <= r)
        B[k++] = A[j++];

    for ( int i = l; i < k; i ++ )
        {
            A[i] = B[i];
        }
}

void merge_sort ( int l, int r, int A[], int B[] )
{
    if (r - l < 1)
        return;

    int middle = (l + r + 1) / 2;

    merge_sort(l, middle - 1, A, B);
    merge_sort(middle, r, A, B);

    merge_array(l, middle, r, A, B);
}

// w rzeczywistości wrapper dla `merge_sort'
void merget ( int * array, int size )
{
    int * B = new int [size];

    // musimy użyć `merge_sort' w takiej postaci
    // aby można było wywołać go rekursywnie
    merge_sort(0, size-1, array, B);

    delete [] B;
}

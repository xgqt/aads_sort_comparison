#include "bubble.hpp"


void bubble ( int * array, int size )
{
    // dla każdego indeksu tablicy
    for ( int i = 0; i < size; i ++ )
        // dla każdego indeksu mniejszego od bierzącego indeksu (na lewo)
        for ( int j = 0; j < size - i - 1; j ++ )

            // jeśli pod bierzącym indeksem jest wartość większa
            // od wartości na prawo (z większym indeksem)
            // są one zamieniane miejscami
            if ( array[j] > array[j+1] )
                std::swap(array[j], array[j+1]);
}

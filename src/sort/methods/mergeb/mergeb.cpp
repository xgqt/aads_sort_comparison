/// Merge Bottom-Up


#include "mergeb.hpp"


// mniejsza wartość z dwóch
int min ( int x, int y )
{
    return ( ( x < y ) ? x : y );
}

// funkcja do złączenia list array[l..m] array[m+1..r] z array[]
void merge ( int array[], int l, int m, int r )
{
    int i;
    int j, k;
    int n1 = m - l + 1;
    int n2 = r - m;

    // tymczasowe listy
    int * L = new int [n1];
    int * R = new int [n2];

    // skopiowanie wartości do tymczasowych list L[] i R[]
    for ( i = 0; i < n1; i ++ )
        L[i] = array[l + i];

    for ( j = 0; j < n2; j ++ )
        R[j] = array[m + 1+ j];

    // złączenie tymczasowych list do `array`
    i = 0;
    j = 0;
    k = l;

    while ( i < n1 && j < n2 )
        {
            if ( L[i] <= R[j] )
                {
                    array[k] = L[i];
                    i ++;
                }
            else
                {
                    array[k] = R[j];
                    j ++;
                }

            k ++;
        }

    // skopiuj pozostałe elementy L[]
    while ( i < n1 )
        {
            array[k] = L[i];
            i ++;
            k ++;
        }

    // skopiuj pozostałe elementy R[]
    while ( j < n2 )
        {
            array[k] = R[j];
            j ++;
            k ++;
        }
}

void mergeb ( int * array, int size )
{
    // do przechowywania długości sublist do scalenia
    // od 1 do n/2
    int curr_size;

    // do przechowywania początkowego indeksu podlisty z lewej strony
    int left_start;

    // złączenie list
    // najpier o rozmiarze 1, potem 2, 3,...
    for ( curr_size = 1; curr_size <= size-1; curr_size = 2*curr_size )
        {
            // znajdż początkowy indeks różnych podlist
            for ( left_start = 0; left_start < size-1; left_start += 2*curr_size )
                {
                    // znajdż kończowy indeks lewej podlisty,
                    // mid+1 to początek prawej podlisty
                    int mid = min(left_start + curr_size - 1, size-1);

                    int right_end = min(left_start + 2*curr_size - 1, size-1);

                    // złaczenie podlist
                    // array[left_start...mid] i array[mid+1...right_end]
                    merge(array, left_start, mid, right_end);
                }
        }
}

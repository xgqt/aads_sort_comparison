#include "select.hpp"


void select ( int * array, int size )
{
    int min;

    for ( int i = 0; i < size-1; i ++ )
        {
            // indeks bierzącego elemntu
            min = i;

            for ( int j = i + 1; j < size; j ++ )
                if ( array[j] < array[min] )
                    min = j;

            std::swap(array[i], array[min]);
        }
}

#include "insert.hpp"


void insert ( int * array, int size )
{
    int tmp;
    int j;

    for ( int i = 1; i < size; i ++ )
        {
            // tmp będzie wartością array[i]
            tmp = array[i];
            j = i;

            while ( j > 0 && array[j-1] > tmp )
                {
                    array[j] = array[j-1];
                    j --;
                }

            // wstawienie tmp we włąsciwe miejsce
            array[j] = tmp;
        }
}

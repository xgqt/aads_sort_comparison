#include "quick.hpp"


int partition ( int * array, int lower, int higher )
{
    // pivot
    int pivot = array[higher];
    // Indeks
    int i = (lower - 1);

    for ( int j = lower; j <= higher - 1; j ++ )
    {
        // Jeśli reraz rozpatrywany element jest mniejszy od pivota
        if ( array[j] < pivot )
        {
            // zwiększ indeks niejszego elementu
            i++;
            std::swap(array[i], array[j]);
        }
    }

    std::swap(array[i + 1], array[higher]);

    return (i + 1);
}

void quicksort ( int * array, int lower, int higher )
{
    if ( lower < higher )
    {
        // pi to indeks partycji
        int pi = partition(array, lower, higher);

        // posortuj elementy przed partycją
        quicksort(array, lower, pi - 1);

        // posortuj elementy po portycji
        quicksort(array, pi + 1, higher);
    }
}

// w rzeczywistości wrapper dla `quicksort'
void quick ( int * array, int size )
{
    // musimy użyć `quicksort' w takiej postaci
    // aby można było wywołać go rekursywnie
    quicksort(array, 0, size-1);
}
